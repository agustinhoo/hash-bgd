# DISCLAIMER

This _terraforming_ uses **Digital Ocean** as a provider. I'm using it because it was the only provider left for me with a free tier.
I'm providing my _api key_ since I do not believe you guys have one to test.

This same reason is why I'm commiting _state_ files.

I wished I could implement a CI/CD pipeline, but sadly I had not time to. My apologies

The blue-green deployment is achieved by running a simple set of commands (hence my wish to use a pipeline) that will be described below.

Also I found more practical to use 2 baked images (based on nginx) to present the scenario, since the images sent by Hash do not reflect 2 different versions.

Tested using:

```
Terraform v0.12.20
+ provider.digitalocean v1.14.0
+ provider.kubernetes v1.10.0
+ provider.local v1.4.0
```

```
Kubernetes
Client Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.9"}
Server Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.9"}
```

## Running

### Creating
`terraform plan`

`terraform apply`

#### STRANGE BUG
source: https://github.com/terraform-providers/terraform-provider-kubernetes/issues/176
workround: ``kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous``

### Deleting
`terraform destroy`

## Testing blue-green deployment

### Apply resources
`kubectl --kubeconfig kubeconfig apply -f nginx_green.yml`
`kubectl --kubeconfig kubeconfig apply -f nginx_blue.yml`
`kubectl --kubeconfig kubeconfig apply -f nginx_service.yml`

### Patching loadbalancer to reflect service changes to version 2
`kubectl --kubeconfig kubeconfig patch service nginx-lb --patch "$(cat lb-patch-blue.yaml)"`

### Patching loadbalancer to reflect service changes back to version 1
`kubectl --kubeconfig kubeconfig patch service nginx-lb --patch "$(cat lb-patch-green.yaml)"`