resource "local_file" "kubeconfig" {
  content  = digitalocean_kubernetes_cluster.cluster.kube_config[0].raw_config
  filename = "${path.module}/kubeconfig"
  provisioner "local-exec" {
    command    = "kubectl --kubeconfig ${path.module}/kubeconfig create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous"
  }
}
