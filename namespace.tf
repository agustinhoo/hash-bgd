
resource "kubernetes_namespace" "default-namespace" {
  metadata {
    name = var.cluster-name
    labels = {
      env = "default"
    }
  }
  depends_on = [local_file.kubeconfig]
}
