do_token = "REDACTED"
cluster-name = "hash-k8s"
cluster-region = "nyc3"
cluster-version = "1.15.9-do.0"
cluster-node-pool-name = "hash-k8s-worker-pool"
cluster-node-pool-size = "s-1vcpu-2gb"
cluster-node-pool-node-count = 3
