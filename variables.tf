variable "do_token" {
}

variable "cluster-name" {
}

variable "cluster-region" {
}

variable "cluster-version" {
}

variable "cluster-node-pool-name" {
}

variable "cluster-node-pool-size" {
}

variable "cluster-node-pool-node-count" {
}

